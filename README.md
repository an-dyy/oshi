# oshi
An asynchronous osu API wrapper made in python 

## Example(s)

   With context manager. I recommend using this as it handles logging in and as well as closing the session.
   ```py
   import asyncio

   import oshi


   async def main() -> None:
      auth = oshi.Authentication(CLIENT_ID, "CLIENT_SECRET")

      async with oshi.Client(auth) as client:
         beatmap = await client.get_beatmap(id=3024187)  # Note: get_beatmap can return None
         print(beatmap.url)

   asyncio.run(main())

   ```

   Without context manager. Useful for when you need to use client in multiple places.
   You will need to manually login and close
   ```py
   import asyncio

   import oshi


   async def main() -> None:
      auth = oshi.Authentication(CLIENT_ID, "CLIENT_SECRET")
      client = oshi.Client(auth)
      
      await client.login()

      beatmap = await client.get_beatmap(id=3024187)
      print(beatmap.url)

      await client.close()

   asyncio.run(main())

   ```

## Development
_For developers_
If you plan on contributing please open an issue beforehand

## Contributors

- [an-dyy](https://github.com/an-dyy) - creator and maintainer

